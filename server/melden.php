<?php

require __DIR__ . '/vendor/autoload.php';


if (isset($_FILES['file'])) {
	if (!filter_var($_REQUEST['mail'], FILTER_VALIDATE_EMAIL))  throw new \Exception('Email not valid!');

	$device = preg_replace('/[^A-Za-z0-9_-]/', '', $_REQUEST['device']);

	$targetDir = __DIR__ . '/meldungen/' . $_REQUEST['mail'] . '/' . $device;
	@mkdir($targetDir, 0770, true);


	$targetBase = $targetDir . '/meldung_' . date('Y-m-d-His'). "_" . rand(1000,9999);
	$targetFile = $targetBase . '.' . (in_array($_FILES['file']['type'], ['image/jpeg', 'image/jpg']) ? 'jpg' : str_replace('image/', '', $_FILES['file']['type']));
	$targetData = $targetBase . '.json';

	move_uploaded_file($_FILES['file']['tmp_name'], $targetFile);

	file_put_contents($targetData, json_encode([
			'date' => date('d.m.Y H:i:s'),
			'name' => $_REQUEST['name'],
			'mail' => $_REQUEST['mail'],
			'coords' => $_REQUEST['coords'],
			'device' => $_REQUEST['device'],
			'message' => $_REQUEST['message'],
			'attachment' => $targetFile,
			'file' => $_FILES['file']
	]));

	if (!file_exists($targetDir . '/approved')) {

		// Create the Transport
		$transport = (new Swift_SmtpTransport('mail.tillmitsch.steiermark.at', 25));

		// Create the Mailer using your created Transport
		$mailer = new Swift_Mailer($transport);

		$approveLink = 'http://gemeindeapp.coreoil.net/apps/tillmitsch/server/melden.php?approve=true&device='.rawurlencode($_REQUEST['device']) . '&mail=' . rawurlencode($_REQUEST['mail']);

		// Create a message
		$message = (new Swift_Message('Foto-Upload freischalten.'))
		  ->setFrom(['app@tilmitsch.gv.at' => 'Tillmitsch-App'])
		  ->setTo([$_REQUEST['mail'] => $_REQUEST['name']])
		  ->setBody('Danke für Ihr Interesse,<br><br>bitte schalten Sie Ihre Email-Addresse frei indem Sie auf den folgenden Link clicken.<br><a href="'.$approveLink.'" >' . $approveLink . '</a>.<br><br> Ihre Tillmitsch-App ;)', 'text/html');
		  ;

		// Send the message
		$result = $mailer->send($message);

		header('Content-Type: application/json; charset=UTF-8');
		echo json_encode(['error' => 0, 'message' => 'Sie haben ein Bestätigungsmail erhalten! Bitte prüfen Sie auch den Spam!']);
		exit;
	}

}

$mail = $_REQUEST['mail'];
$device = preg_replace('/[^A-Za-z0-9_-]/', '', $_REQUEST['device']);
$targetDir = __DIR__ . '/meldungen/' .$mail . '/' . $device;

if (@$_REQUEST['approve']) {
	if (file_exists($targetDir)) {
		file_put_contents($targetDir . '/approved', date('Y-m-d H:i:s'));
	}
}

if (!file_exists($targetDir . '/approved')) {
	header('Content-Type: application/json; charset=UTF-8');
	echo json_encode(['error' => 1, 'message' => 'Mail nicht bestätigt!']);
	exit;
}

foreach(glob($targetDir. '/meldung_*.json') as $file) {

	$maildata = json_decode(file_get_contents($file), true);
	if (!$maildata['mail']) continue;

	// Create the Transport
	$transport = (new Swift_SmtpTransport('mail.tillmitsch.steiermark.at', 25));

	// Create the Mailer using your created Transport
	$mailer = new Swift_Mailer($transport);

	// Create a message
	$message = (new Swift_Message('Neuer Foto-Upload'))
	  ->setFrom(['app@tilmitsch.gv.at' => 'Tillmitsch-App'])
	  ->setTo(['amtsleiter@tilmitsch.gv.at' => 'Amtsleiter'])
		->setReplyTo([$maildata['mail'] => $maildata['name']])
	  ->setBody('<dl><dt>User</dt><dd>' . htmlentities($maildata['name']) .  ' &lt;' . htmlentities($maildata['mail']) . '&gt;<dd><dt>Nachricht<dt><dd>' . htmlentities($maildata['message']) . '</dd><dt>Coordinaten<dt><dd>' . htmlentities($maildata['coords']) . '</dd></dl>', 'text/html')
		->attach(Swift_Attachment::fromPath($maildata['attachment'])->setFilename(basename($maildata['attachment'])))
	  ;
 $result = $mailer->send($message);

	unlink($file);
	unlink($maildata['attachment']);

	// Send the message

}


if (@$_REQUEST['approve']) {
	header('Content-Type: text/html; charset=UTF-8');
	echo '<html>';
	echo '<head>';
		echo '<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">';
		echo '<meta charset="utf-8">';
		echo '<meta name="apple-mobile-web-app-capable" content="yes">';
		echo '</head>';
	echo '<body style="background: #eee;">';
	echo '<div style="width: 240px; margin-left: auto; margin-right: auto; margin-top: 10vh; background: #fff; padding: 2em;">';
	echo '<img src="../images/icon.png" style="width: 100%;"/>';
	echo '<h1>Danke für Ihr Interesse!</h1>';
	echo '<p>Wir kümmern uns so schnell wie möglich um Ihr Anliegen</p>';
	echo '</div>';
	echo '</body>';
	echo '</html>';
} else {
	header('Content-Type: application/json; charset=UTF-8');
	echo json_encode(['error' => 0, 'message' => 'Ihr Anliegen wurde zugestellt!']);
}
