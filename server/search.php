<?php

require_once('elasticsearch.php');
header('Content-Type: application/json; charset=utf8');
header('Access-Control-Allow-Origin: *');

$queryString = isset($_GET['q']) ? $_GET['q'] : 'spar';

$queryTypes = array(
        "fuzzy_like_this" => [
                "fields" => [ "idx_ti", "idx_in" ],
                "like_text" => $queryString,
                "fuzziness" => 0.8
         ],
        "wildcard" => [
                "idx_ti" => "*$queryString*",
                "idx_in" => "*$queryString*"
        ]
);

foreach($queryTypes as $qType => $qTypeSettings) {
$query = [
        'query' => [
                $qType => $qTypeSettings
        ],
        'highlight' => [ 'fields' => [ "idx_ti" => new stdClass, "idx_in" => new stdClass ] ],
];

$index = json_encode($query);

        $chlead = curl_init();
        curl_setopt($chlead, CURLOPT_URL, $ELASTICSERVER.'/_search?size=30');
        curl_setopt($chlead, CURLOPT_USERAGENT, 'Coreoil-Searcher/1.0');
        curl_setopt($chlead, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: '.strlen($index)));
        curl_setopt($chlead, CURLOPT_VERBOSE, 1);
        curl_setopt($chlead, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($chlead, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($chlead, CURLOPT_NOBODY, false);
        curl_setopt($chlead, CURLOPT_POSTFIELDS,$index);
        curl_setopt($chlead, CURLOPT_SSL_VERIFYPEER, 0);
        $chleadresult = curl_exec($chlead);
        $chleadapierr = curl_errno($chlead);
        $chleaderrmsg = curl_error($chlead);
        curl_close($chlead);

        $result = json_decode($chleadresult);

$searchresult = [ 'ti' => 'Suchergebnis', 'd' => [] , 'qtype' => $qType ];
foreach($result->hits->hits as $key => $entry) {
        $searchEntry = $entry->_source;
        unset($searchEntry->in);
        unset($searchEntry->idx_in);

        foreach($entry->highlight as $field => $hit) {
                $searchEntry->to = strip_tags(reset($hit), '<em>');
                break;
        }


        $searchresult['d'][] = $searchEntry;
}

if (count($searchresult['d'])> 0 || $queryString == "") {
        echo json_encode($searchresult);
        exit;
}
}

echo json_encode($searchresult);

