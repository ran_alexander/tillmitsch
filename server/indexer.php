<?php
require_once('elasticsearch.php');

$Directory = new RecursiveDirectoryIterator($INDEXDIR);
$Iterator = new RecursiveIteratorIterator($Directory);
$Regex = new RegexIterator($Iterator, '/^.*\/[0-9]+\.json$/i', RegexIterator::GET_MATCH);

header('Content-Type: text/plain; charset=utf-8');
exec("curl -XDELETE '$ELASTICSERVER'");
foreach($Regex as $file) {
        $fileHash = md5($file[0]);
        $json = json_decode(file_get_contents($file[0]));
        $json->d[0]->hr = preg_replace('/\.json$/', '', str_replace($INDEXDIR, basename($INDEXDIR), $file[0]));

        foreach( ['in', 'ti'] as $field) {
                foreach(array('idx_' => false, 'idx_sep_' => true) as $prefix => $separated) {
                        $idxField = $prefix.$field;
                        $idxContent = $json->d[0]->$field;

                        if ($separated) {
                                $idxContent = str_replace(['&shy;', json_decode('"\u00ad"')], [' ', ' '], $idxContent);
                        } else {
                                $idxContent = str_replace(['&shy;', json_decode('"\u00ad"')], ['', ''], $idxContent);
                        }
                        $idxContent = strip_tags($idxContent);
                        $idxContent = trim($idxContent);
                        $idxContent = html_entity_decode($idxContent, null, 'UTF-8');

                        $json->d[0]->$idxField = $idxContent;
                }
        }
        unset($json->d[0]->in);
        $json->d[0]->sort_date = time();

        $index = json_encode($json->d[0]);


        $chlead = curl_init();
        curl_setopt($chlead, CURLOPT_URL, $ELASTICSERVER.'/'.$fileHash);
        curl_setopt($chlead, CURLOPT_USERAGENT, 'Coreoil-Indexer/1.0');
        curl_setopt($chlead, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: '.strlen($index)));
        curl_setopt($chlead, CURLOPT_VERBOSE, 1);
        curl_setopt($chlead, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($chlead, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($chlead, CURLOPT_POSTFIELDS,$index);
        curl_setopt($chlead, CURLOPT_SSL_VERIFYPEER, 0);
        $chleadresult = curl_exec($chlead);
        $chleadapierr = curl_errno($chlead);
        $chleaderrmsg = curl_error($chlead);
        curl_close($chlead);
        echo $file[0] . " - " . $chleadresult . "\n";

}
