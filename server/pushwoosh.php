<?php
define('PW_AUTH', 'TYj6FRAieCwWD80DtZATPLbrKP6a3TPHZjKtzoUje66Ls7EQRdtCeF1wF8bBIbSoNAM38ToyUWg7F3Dmv4wZ');
define('PW_APPLICATION', '3D78F-9074F');
define('PW_DEBUG', true);
 
function pwCall($method, $data) {
    $url = 'https://cp.pushwoosh.com/json/1.3/' . $method;
    $request = json_encode(['request' => $data]);
 
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
 
    $response = curl_exec($ch);
    $info = curl_getinfo($ch);
    curl_close($ch);
 
    if (defined('PW_DEBUG') && PW_DEBUG) {
        print "[PW] request: $request\n";
        print "[PW] response: " . print_r($response, true) ."\n";
        print "[PW] info: " . print_r($info, true);
    }
 
 
pwCall('createMessage', [
    'application' => PW_APPLICATION,
    'auth' => PW_AUTH,
    'notifications' => [
            [
                'send_date' => 'now',
                'content' => 'test',
                'data' => ['path' => '#/settings'],
                 /* iOS related */
                "ios_badges" => "+1",
                "ios_sound" => "default",
                "ios_ttl" => 3600, 
                /* Android related */
                "android_header" => "Hitzendorf", 
                "android_icon" => "icon",
                "android_gcm_ttl" => 3600
            ]
        ]
    ]
);