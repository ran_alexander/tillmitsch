<?php

ini_set('display_errors', 1);
error_reporting(E_ALL);

$reuqest = preg_replace('/\?.*$/', '', $_SERVER['REQUEST_URI']);

$filename = '.'.str_replace('.ics', '', $reuqest).'.json';

$filecontent = preg_replace('/\x{AD}/u', '', file_get_contents($filename));
$filecontent = preg_replace('/&shy;/', '', $filecontent);
$contentList = json_decode($filecontent);

header('Content-Type: text/calendar; charset=UTF-8');
//header('Content-Type: text/plain; charset=UTF-8');

$filter = array();

if (preg_match('/\?/', $_SERVER['REQUEST_URI'])) {
        $qp = parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY);
        parse_str($qp, $_GET);

        $filter = isset($_GET['filter']) ?  $_GET['filter'] : array();

}

$contents = array();
while(($content = array_shift($contentList->d))) {

        if (!isset($content->da)) {
                if (!isset($content->hr)) {
                        continue;
                }

                $filename = './v1/'.$content->hr.'.json';
                $filecontent = preg_replace('/\x{AD}/u', '', file_get_contents($filename));
                $filecontent = preg_replace('/&shy;/', '', $filecontent);
                $sublist = json_decode($filecontent);

                foreach($sublist->d as $entry) {
                        unset($entry->hr);
                        $contentList->d[] = $entry;
                }
                continue;
        }

        foreach($filter as $key => $value) {
                if (isset($content->$key) && !preg_match('/'.$value.'/i', $content->$key)) {
                        continue 2;
                }
        }
        $contents[] = $content;
}

?>
BEGIN:VCALENDAR
VERSION:2.0
PRODID:net.coreoil.gemeindeapp
METHOD:PUBLISH
<?php
foreach($contents as $content) {
        $dateFormat = "Ymd\THisT";
        $startDate = strtotime($content->da . ' ' . ($content->tm ? $content->tm : '00:00:00'));
        if ($content->tm) {
                $endDate = $startDate + 3600;
        } else {
                $endDate = $startDate + (3600 * 24);
        }
        ?>
BEGIN:VEVENT
UID:<?php echo md5(var_export($content, true)) ?>@gemeindeapp.coreoil.net
ORGANIZER;CN="<?php echo $content->to; ?>":MAILTO:<?php $arrParts = explode('/', $filename); echo $arrParts[2]; ?>@gemeindeapp.coreoil.net
LOCATION:<?php echo trim($content->lo) . "\n"; ?>
SUMMARY:<?php echo $content->ti . "\n"; ?>
DESCRIPTION:<?php if(isset($content->hr) && preg_match('/^http/', $content->hr)) echo $content->hr; echo "\n"; ?>
CLASS:PUBLIC
DTSTART:<?php echo date($dateFormat, $startDate) . "\n"; ?>
DTEND:<?php echo date($dateFormat, $endDate) . "\n"; ?>
DTSTAMP:<?php echo date($dateFormat) . "\n"; ?>
END:VEVENT
<?php
}
?>
END:VCALENDAR
