var pushDisabled = false;

function initPushwoosh() {
    if (pushDisabled) return;
    
    if (!window.plugins || !window.plugins.pushNotification) return;
    
    
	var pushNotification = window.plugins.pushNotification;
	if(device.platform == "Android")
	{
		registerPushwooshAndroid();
	}

	if(device.platform == "iPhone" || device.platform == "iOS")
	{
		registerPushwooshIOS();
	}
        
}

function showToken() {
    navigator.notification.alert($('#devicetoken').attr('data-token'), function() {}, "Tillmitsch" );
    return false;
}

function showDeviceToken(devicetoken) {
    $('#devicetoken').attr('data-token', devicetoken);
}

/** 
 * --------------------------------------------------
 *                  Android
 * --------------------------------------------------
 */

function registerPushwooshAndroid() {

 	var pushNotification = window.plugins.pushNotification;

	//set push notifications handler
	document.addEventListener('push-notification',
		function(event)
		{
            var title = event.notification.title;
            var params = event.notification.userdata || "{}";
            if (typeof params == "string") {
                params = JSON.parse(params);
            }
            
            if (params.path) window.location.hash = params.path; $(window).trigger('hashchange');
		}
	);
	
    pushNotification.registerDevice({ projectid: "XXXXX", appid : "3D78F-9074F" },
		function(token)
		{
			onPushwooshAndroidInitialized(token);
		},
		function(status)
		{
            if(appstarted){
			navigator.notification.alert("Pushbenachrichtigungen konnten nicht registiert werden. status: " +  status, function() {}, "Tillmitsch");
            }
		   
		}
	);
    
	setTimeout(function() { pushNotification.onDeviceReady(); }, 200);
}

function onPushwooshAndroidInitialized(pushToken)
{
    showDeviceToken(pushToken);

	//set multi notificaiton mode
	pushNotification.setMultiNotificationMode();
	pushNotification.setEnableLED(true);

	//set single notification mode
	//pushNotification.setSingleNotificationMode();

	//disable sound and vibration
	//pushNotification.setSoundType(1);
	//pushNotification.setVibrateType(1);

	pushNotification.setLightScreenOnNotification(false);

	//goal with count
	//pushNotification.sendGoalAchieved({goal:'purchase', count:3});

	//goal with no count
	//pushNotification.sendGoalAchieved({goal:'registration'});

	//setting list tags
	//pushNotification.setTags({"MyTag":["hello", "world"]});

	//Pushwoosh Android specific method that cares for the battery
	pushNotification.startGeoPushes();
}

/** 
 * --------------------------------------------------
 *                  IOS
 * --------------------------------------------------
 */ 
function registerPushwooshIOS() {
 	var pushNotification = window.plugins.pushNotification;

 	//set push notification callback before we initialize the plugin
	document.addEventListener('push-notification',
		function(event)
		{
			//get the notification payload
			var notification = event.notification;

            var params =  notification.u || {};
            
			//display alert to the user for example
			navigator.notification.alert(notification.aps.alert, function() {if (params.path) window.location.hash = params.path; $(window).trigger('hashchange'); }, "Tillmitsch");

			//clear the app badge
			pushNotification.setApplicationIconBadgeNumber(0);
		}
	);

	//register for pushes.
	pushNotification.registerDevice({alert: true, badge: true, sound: true , pw_appid:"XXXXX", appname:"Tillmitsch"},
		function(status)
		{
			var deviceToken = status['deviceToken'];
			console.warn('registerDevice: ' + deviceToken);
			onPushwooshiOSInitialized(deviceToken);
		},
		function(status)
		{
			navigator.notification.alert("Pushbenachrichtigungen konnten nicht registiert werden. status: "+ JSON.stringify(status), function() {}, 'Gralla');
			
		}
	);

    //trigger pending push notifications
	setTimeout(function() { pushNotification.onDeviceReady(); }, 200);
    
	//reset badges on start
	pushNotification.setApplicationIconBadgeNumber(0);
}

function onPushwooshiOSInitialized(pushToken)
{
	showDeviceToken(pushToken);
}


function handleSettings() {
    $('#settingscontainer').addClass('active');

    $('#settings .listicon:not(.fixed)').removeClass('fa-check-square').addClass('fa-square');
    
    if (pushDisabled) return;
    
    if (!window.plugins || !window.plugins.pushNotification) {
        return;
    }
    var pushNotification = window.plugins.pushNotification;

    pushNotification.getTags(function(tags) {
        if (!tags.Bereiche) return;
        for(var ofs in tags.Bereiche) {
            var value = tags.Bereiche[ofs];
            $('#settings .listicon[data-value='+value+']').addClass('fa-check-square');
        }
        
        var bereicheTag = [];
    
        $('#settings .listicon').each(function() {
            if ($(this).hasClass('fa-check-square')) {
                bereicheTag.push($(this).attr('data-value'));
            }
        });

        var pushNotification = window.plugins.pushNotification;
        pushNotification.setTags({"Bereiche" : bereicheTag }, function() {}, function(message) { if( message == "invalid action") return; alert('error ' +JSON.stringify(message)); });
            
    }, function(message) {
        if(appstarted){
            navigator.notification.alert('Pushbenachrichtigungstags können nicht geholt werden. Bitte stellen Sie sicher, dass Gralla in den Push-Berechtigungen Ihres Smartphones aktiviert ist.', function() {}, "Tillmitsch");
        }
    });
}

function pushToggleTag($tag) {
    if (!$tag.hasClass('fixed')) {
        if ($tag.hasClass('fa-check-square')) {
            $tag.addClass('fa-square').removeClass('fa-check-square');
        } else {
            $tag.addClass('fa-check-square').removeClass('fa-square');
        }
    }
    
    if (pushDisabled) return;
    
    if (!window.plugins || !window.plugins.pushNotification) {
        return;
    }
    
    var bereicheTag = [];
    
    $('#settings .listicon').each(function() {
        if ($(this).hasClass('fa-check-square')) {
            bereicheTag.push($(this).attr('data-value'));
        }
    });
    
    var pushNotification = window.plugins.pushNotification;
    pushNotification.setTags({"Bereiche" : bereicheTag }, function() {}, function(message) { if( message == "invalid action") return; alert('error ' +JSON.stringify(message)); });
}
