document.createElement("header");
document.createElement("nav");
document.createElement("section");
document.createElement("section");
document.createElement("footer");

var highlightColor = "#518374";
var highlightLightColor = "#f13432";

var appName = "Tillmitsch";

var iconmapping = {
    'firmenliste': 'fa fa-sitemap',
    'gastronomie-und-beherbergung': 'fa fa-coffee',
    'gesundheit-und-soziales': 'fa fa-heart',
    'freizeit-und-sport': 'fa fa-bicycle',
    'verkehr-und-fortbewegung': 'fa fa-bus',
    'offentliche-einrichtungen-und-behorden': 'fa fa-bank'

};

var historyOffset = 0;
var comesFromHistory = false;
var appstarted = true;
var showLoader;

var resizeEnd = function() { };

var historyBack = function () {
    var thisUrl = window.location.hash.replace('#', '');
    if (thisUrl == "nav" || thisUrl == "") {
        if (navigator.app){
            appstarted = false;
            navigator.app.exitApp();
        }

    } else {
        comesFromHistory = true;
        historyOffset -= 2;
        history.back();
    }
};


var headerBgs = function() {
    $('section.active .backgroundfading div:last()').css({
        "background-image": "url(images/header_0"+ parseInt(1 + Math.random()*7)+ ".jpg)",
        'background-position': 'center center',
        'background-size': "cover"
    });

};

var baseUrl = 'http://gemeindeapp.coreoil.net/v1';

function is_touch_device() {
  try {
    document.createEvent("TouchEvent");
    return true;
  } catch (e) {
    return false;
  }
}

function get_click_event() {
    return (typeof window.ontap ? 'click' : 'tap');
}

var helpVersion = 1;

function nextHelp() {
    var thisActive = $('#help > .helpscreen.active');
    thisActive.removeClass('right').addClass('left').removeClass('active');
    var nextActive = thisActive.next('div.helpscreen');
    nextActive.addClass('active');

    var bgurl = nextActive.attr('data-bg');
    if (bgurl) {
        window.location.replace(bgurl);
        $(window).trigger('hashchange');
    }

    if (nextActive.length == 0) {
        $('#help').removeClass('active');
        setTimeout(function() {
            $('#help > .helpscreen').removeClass('active left').addClass('right');
            $('#help > .helpscreen:eq(0)').addClass('active');
            $('#help .indicator').removeClass('active');
            $('#help .indicator:eq(0)').addClass('active');

        }, 500);
    } else {
        var ofs = nextActive.index('#help > .helpscreen');
        $('#help .indicator').removeClass('active');
        $('#help .indicator:eq('+ofs+')').addClass('active');
    }
    window.localStorage.setItem('hideHelp', helpVersion);
}
function prevHelp() {
    var thisActive = $('#help > .helpscreen.active');
    var nextActive = thisActive.prev('div.helpscreen');
    if (nextActive.length == 0) {
        return;
    }
    thisActive.removeClass('left').addClass('right').removeClass('active');
    nextActive.addClass('active');

    var ofs = nextActive.index('#help > .helpscreen');
    $('#help .indicator').removeClass('active');
    $('#help .indicator:eq('+ofs+')').addClass('active');

    var bgurl = nextActive.attr('data-bg');
    if (bgurl) {
        window.location.replace(bgurl);
        $(window).trigger('hashchange');
    }

    window.localStorage.setItem('hideHelp', helpVersion);
}


var searchUrl = "http://gemeindeapp.coreoil.net/apps/tillmitsch/server/search.php?q=";




var startApp = function () {

    baseUrl = 'http://gemeindeapp.coreoil.net/v1';

    if (document.addEventListener) document.addEventListener("backbutton", historyBack, false);
    if (window.device) {
        //$('body').addClass(window.device.platform.toLocaleLowerCase());
    }

    /** init push */
    initPushwoosh();

    if (window.cordova) {
        StatusBar.backgroundColorByHexString("#333");
        StatusBar.overlaysWebView(false);
    }

    var mapsUrl = window.device && window.device.platform == "iOS" ? 'http://maps.apple.com/maps' : 'http://maps.google.com/maps';
    var menus = [];

    var scrollNav = new iScroll('scroll-nav');
    var scrollList = new iScroll('scroll-list');
    var scrollDetail = new iScroll('scroll-detail');
    var scrollSearch = new iScroll('scroll-search');
    var scrollSettings = new iScroll('scroll-settings');

    var orientation = function () {
        if (window.innerWidth > window.innerHeight) {
            $('body').removeClass('portrait').addClass('landscape');
        } else {
            $('body').removeClass('landscape').addClass('portrait');
        }
    }

    orientation();

    resizeEnd = function () {
        orientation();
        scrollNav.refresh();
        scrollList.refresh();
        scrollDetail.refresh();
        scrollSearch.refresh();
        scrollSettings.refresh();
    };


    var resizeEndTimeout = false;
    $(window).resize(function () {
        if (resizeEndTimeout) clearTimeout(resizeEndTimeout);
        resizeEndTimeout = setTimeout(resizeEnd, (typeof cordova == "undefined" ? 300 : 0));
    });

    $.ajaxSetup({ timeout: 5000 });

    var initNav = function() {
         $.get(baseUrl + '/tillmitsch.json', function (data) {
            menus = data.d;

            $('#navcontainer header .back').attr('data-impress', data.i);


            var currentHash = window.location.hash ? '#' + window.location.hash.replace('#', '') : window.location.hash;

            var icons = {
                'News': 'fa fa-inbox',
                'Veranstaltungen': 'fa fa-calendar',
                'Mülltermine': 'fa fa-trash',
                'Kontakte': 'fa fa-group',
                'In der Nähe': 'fa fa-map-marker',
                'Behördenweg': 'fa fa-university'
            };

            for (var ofs = 0; ofs < menus.length; ofs++) {
                var icn = "fa fa-shield";
                if (menus[ofs].ic) {
                    icn = menus[ofs].ic;
                } else if (icons[menus[ofs].ti]) {
                    icn = icons[menus[ofs].ti];
                }
                $('nav').append(
                    '<a href="#/' + menus[ofs].hr + '" title="' + encodeURIComponent(menus[ofs].ti) + '">' +
                    '<i class="' + icn + '"></i> ' +
                    '<span>' + menus[ofs].ti + '</span>' +
                    '</a>'
                );
            }

           $('nav').append(
                '<a href="javascript:onPhotoInit();" title="Foto-Upload">' +
                '<i class="fa fa-camera"></i> ' +
                '<span>Foto-Upload</span>' +
                '</a>'
            ).append(
                '<a href="#/search" title="Suche">' +
                '<i class="fa fa-search"></i> ' +
                '<span>Suche</span>' +
                '</a>'
            ).append(
                '<a href="javascript:$(\'#help\').addClass(\'active\');">' +
                '<i class=" fa fa-question-circle"></i> ' +
                '<span>Hilfe</span>' +
                '</a>'
            );;


			$(document).on('app:mainMenu', function() {
				scrollNav.refresh();

				setTimeout(function () {
					window.location.hash = '#nav';
					$(window).trigger('hashchange');
				}, 0);
				if (currentHash) {
					setTimeout(function () {
						window.location.hash = currentHash;
						$(window).trigger('hashchange');
					}, 700);
				}
				$('#loading').hide();
				splashstate('loadingmenu');
			});
			$(document).trigger('app:mainMenu');

        }).fail(function (jqXHR, textStatus, message) {
            $('#loading').show();
            if (navigator.notification && navigator.notification.alert && appstarted) {
                var ext = function() {
                    if (window.device && (window.device.platform == "Android")) {
                        navigator.app.exitApp();
                    } else {
                        setTimeout(initNav, 2000);
                    }
                }
                navigator.notification.alert("Fehler bei der Internetverbindung", ext, appName );
            } else {
                alert("Fehler bei der Internetverbindung.");
               historyBack();
            }
        });

    };

    setTimeout(function () {
        initNav();
        /* initmap(); */
    }, 0);

    var currentHelpVersion = parseInt(window.localStorage.getItem('hideHelp')) || 0;

    if (currentHelpVersion < helpVersion) {
        $('#help').addClass('active');
    }

    $('#help').hammer().on('dragleft', function(ev) {
        ev.gesture.stopDetect();
        nextHelp();
    }).on('dragright', function(ev) {
        ev.gesture.stopDetect();
        prevHelp();
    });

    var clickEvent = get_click_event();

    $('header').hammer().on('dragdown', function(ev) {
        ev.gesture.stopDetect();
        if (window.location.hash.match(/^#\/search/)) {
            $('#searchcontainer').addClass('active');
            headerBgs();
            return;
        }
        window.location.hash = '#/search';
        $(window).trigger('hashchange');
    }).on('dragup', function(ev) {
        ev.gesture.stopDetect();
        if (!window.location.hash.match(/^#\/search/)) return;
        $('#searchcontainer').removeClass('active');
        headerBgs();
    });

    $('#searchfield').keyup(function() {
         $('#searchbutton').attr('href', '#/search/' + $(this).val());
    }).change(function() {
        window.location.replace($('#searchbutton').attr('href'));
        $(window).trigger('hashchange');
    });


    var hidePopup = function() {
        $('.popup').removeClass('active');
        setTimeout(function() { $('.popup').css('display', ''); }, 150);
    };

    $('.popup .btn-cancel').on(clickEvent, function() {
        hidePopup();
    });

    $('#loading').on('tap click', function() {
        $(this).hide();
    });

    $('#reminder_time_save').on(clickEvent, function() {
        hidePopup();
        var $popup = $('#popup_reminder_time');
        var frac = $popup.attr('data-fraction');
        var area = $popup.attr('data-area');

        var tm = $('#reminder_time').val();
        if (!tm || !tm.match(/^[0-9]{1,2}:[0-9]{2}$/)) {
            alert('Uhrzeit ungültig', function() {}, appName);
            return;
        }

        var split = tm.split(':');
        importTrashAction(frac, area, split[0], split[1]);
    });


    $('body').on(clickEvent, 'a.inactive', function (event) {
        event.stopPropagation();
        event.preventDefault();
        return false;
    });

    var timeout = (new Date()).getTime();
    $('body').on(clickEvent, 'a', function (event) {

		if ($(this).hasClass('share') || $(this).hasClass('calendar') || $(this).hasClass('inactive') || $(this).hasClass('ignore')) {
            return false;
		}

        var anchor = $('.detail-leben-in a[name="'+$(this).attr('href').replace('#', '')+'"]');
        if (anchor.length > 0) {
            scrollDetail.scrollToElement(anchor.get(0));
            return false;
        }

        var now = (new Date()).getTime();
        var diff = now - timeout;
        timeout = now;
        if (diff <= 500) {
            event.stopPropagation();
            event.preventDefault();
            return false;
        }

        if ($(this).attr('href').match(/^javascript/)) {
            if ($(this).closest('#settings').length > 0) {
                pushToggleTag($(this).find('.listicon'));
            }
        } else if ($(this).hasClass('inactive')) {
            event.stopPropagation();
            event.preventDefault();
            return false;
        } else {

            if ($(this).attr('href').substring(0, 4) == "http") {
                event.stopPropagation();
                event.preventDefault();

                window.open($(this).attr('href'), '_system');
                return false;
            }
        }

    });

	$('body').on('submit', '#camera-submit-form', function (event) {
		event.stopPropagation();
		event.preventDefault();
		uploadPhoto();
	});
	$('body').on(clickEvent, '.camera_take', function (event) {
		event.stopPropagation();
		event.preventDefault();
		getPhoto(navigator.camera.PictureSourceType.CAMERA);
	});
	$('body').on(clickEvent, '.camera_select', function (event) {
		event.stopPropagation();
		event.preventDefault();
		getPhoto(navigator.camera.PictureSourceType.PHOTOLIBRARY);
	});
	$('body').on(clickEvent, '.camera_submit', function (event) {
		event.stopPropagation();
		event.preventDefault();
		$('#camera-submit-form').submit();
	});
	$('body').on(clickEvent, '.camera_cancel', function (event) {
		event.stopPropagation();
		event.preventDefault();
	});


    $('body').on(clickEvent, '#history_next:not(.inactive)', function (event) {
        if (event.isDefaultPrevented() || event.isPropagationStopped()) return;
        event.stopPropagation();
        event.preventDefault();
        history.forward();
		return false;
    });
    $('body').on(clickEvent, '#history_prev:not(.inactive)', function (event) {
        if (event.isDefaultPrevented() || event.isPropagationStopped()) return;
        event.stopPropagation();
        event.preventDefault();
        historyBack();
		return false;
    });

    $('body').on(clickEvent, 'a.share:not(.inactive)', function (event) {
        event.preventDefault();
		event.stopPropagation();

        var img = $(this).attr('data-img').replace(/\?.*$/, '');
        if (img == "" || img.match(/\%20/)) {
            img = null;
        }
        var subject = $(this).attr('title') || null;
        var url = $(this).attr('href') || null;
        var message = "Schau dir das an! ";

        if (window.plugins && window.plugins.socialsharing) {
            window.plugins.socialsharing.share(message, subject, img, url, function () {}, function (error) {
                alert('ERROR: ' + error);
            });
        } else {
            window.open("mailto:?subject=" + encodeURIComponent(subject) + "&body=" + encodeURIComponent(message + url));
        }


		return false;
    });
    $('body').on(clickEvent, 'a.calendar:not(.inactive)', function (event) {
        event.preventDefault();
		event.stopPropagation();

		var a = $(this);
        var dateParts = $(this).attr('data-start').split(/[- :]/);
        var startDate = new Date(dateParts[0], parseInt(dateParts[1])-1, dateParts[2], dateParts[3], dateParts[4], dateParts[5]);

        dateParts = $(this).attr('data-end').split(/[- :]/);
        var endDate = new Date(dateParts[0], parseInt(dateParts[1])-1, dateParts[2], dateParts[3], dateParts[4], dateParts[5]);

        var title = $(this).attr('title');
        var loc = $(this).attr('data-location');
        var notes = $(this).attr('data-owner') + "\n" + $(this).attr('data-note');

        title = title.replace(/[\n\r]/g, ' ');
        loc = loc.replace(/[\n\r]/g, ' ');
        notes = notes.replace(/[\n\r]/g, ' ');

        // magic troll request
        if ($('#detailtitle').text().trim() == "Abfall") {
            startDate.setMinutes(0);
            startDate.setHours(0);
            startDate.setSeconds(0);
            endDate.setTime(startDate.getTime());
            endDate.setDate(endDate.getDate() + 1);
        }


        var success = function (message) {
            window.plugins.toast.showShortCenter('Kalendereintrag hinzugefügt');
        };
        var error = function (message) {
            var all = {
                'message': message,
                'title': title,
                'location': loc,
                'notes': notes,
                'startDate': startDate,
                'endDate': endDate
            };
            window.plugins.toast.showShortCenter('Kalendereintrag konnte nicht hinzugefügt werden: ' + JSON.stringify(all));
        };

        if (window.plugins && window.plugins.calendar) {
            navigator.notification.confirm(
                "Sind Sie sicher, dass Sie den Termin in Ihren persönlichen Kalender eintragen wollen?",
                function(selection) {
                    if (selection == 1) {

                        var rd = new Date(startDate.getTime() - (60*60*1000));
                        var reminderDateString = rd.getFullYear() + "-" + ("0" + (rd.getMonth() + 1)).substr(-2) + "-" + ("0" + rd.getDate()).substr(-2);
                        reminderDateString+= "T" + ("0"+rd.getHours()).substr(-2) + ":" + ("0"+rd.getMinutes()).substr(-2) + ":00";

                        $('#reminder_datetime').val(reminderDateString);

                        $('#popup_reminder_datetime').show();
                        setTimeout(function() { $('#popup_reminder_datetime').addClass('active'); }, 150);

                        $("#reminder_datetime_save").unbind( clickEvent );
                        $('#reminder_datetime_save').on(clickEvent, function() {
                             setTimeout(function () {
                                a.addClass('inactive');
                            }, 0);


                            hidePopup();
                            var $popup = $('#popup_reminder_datetime');
                            var tm = new Date($('#reminder_datetime').val());
                            tm.setTime(tm.getTime() + tm.getTimezoneOffset() * 60 * 1000)

                            var calOptions = window.plugins.calendar.getCalendarOptions();
                            calOptions.firstReminderMinutes = parseInt((startDate.getTime() - tm.getTime()) / (60 * 1000));

                            window.plugins.calendar.findEvent(title, loc, notes, startDate, endDate, function(eventsFound) {
                                if (eventsFound != null && eventsFound.length > 0) {
                                    window.plugins.toast.showShortCenter(title +' ist bereits in Ihrem Kalender eingetragen');
                                    return;
                                }
                                window.plugins.calendar.createEventWithOptions(title, loc, notes, startDate, endDate, calOptions, success, error);

                            }, function() {
                                window.plugins.toast.showShortBottom('Error in Calendar lookup');
                            });

                        });

                    }
                },
                "Termineintrag",
                ['OK', 'Abbrechen']
            );

        } else {
            window.open($(this).attr('href'), '_system');
        }

		return false;
    });


    $('body').on(clickEvent, '.maprow', function (event) {
        var href = $(this).attr('data-href');
        window.location.hash = href;
    });

    $('body').on(clickEvent, '.back', function (event) {
        event.preventDefault();
        if ($(this).hasClass('helpout')) {
            nextHelp();
            return false;
        }
        var impressLink = $(this).attr('data-impress');
        if (impressLink) {
            window.location.hash = '#/' + impressLink;
            $(window).trigger('hashchange');
        } else {
            historyBack();
        }
    });


    $('body').on(clickEvent, '.openroute:not(.inactive)', function (event) {
        event.preventDefault();
        window.open($(this).attr('href'), '_system');
    });

    $('body').on(clickEvent, '.trashimport:not(.inactive)', function (event) {
        event.preventDefault();
        importTrash($(this).attr('data-fraction'), parseInt($(this).attr('data-area')), $(this).attr('data-message'));
    });

    var month = [
  'Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'
 ];

    var days = [
  'Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'
 ];

    var today = new Date();

    var templates = {
        'list': function (row, isSearch) {
            var link = row.hr;
            var title = row.ti || "";
            var subtitle = row.to || '';
            var date = null;
            var image = row.th || "";
            var type = null;
            var typeData = null;
            var count = null;
            var countIcon = row.ic || null;

            if (row._parent && row._parent.ty == "trash") {
                typeData = null;

                if (title.search('Abfall') >= 0) {
                    typeData = "hd-sammelzentrum";
                } else if (title.search('Alt') >= 0) {
                    typeData = "hd-altpapier";
                } else if (title.search('Gel') >= 0) {
                    typeData = "hd-plastik";
                } else if (title.search('Bio') >= 0) {
                    typeData = "hd-bio";
                } else if (title.search('Rest') >= 0) {
                    typeData = "hd-rest";
                }
                if (typeData) {
                    type = "icon";
                }
            }
            if (row.cn) {
                count = row.cn;
            }

            if (row.da) {
                var dateParts = row.da.split('.');
                date = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);

                title = title.replace(/-[^-]*$/, '');
            }

            var str = "<a class='row' ";

            if (link) {
                if (link.match('#')) {
                    str += "href='" + link + "'";
                } else {
                    str += "href='#/" + link + "'";
                }
            } else if(row.op) {
                str += "href='" + row.op + "' target='_system'";
            }

            str += ">";
            if (image) {
                str += '<div class="image" style="background-image: url(' + image + ');" ></div>';
            } else if (date) {
                var extraClass = "";
                var dT = today.getFullYear() + today.getMonth() + today.getDate();
                var dD = date.getFullYear() + date.getMonth() + date.getDate();

                if (dT == dD) {
                    extraClass = "today";
                }
                str += "<div class='date'>";
                str += "<div class='datebg'>";
                str += "<span class='month " + extraClass + "'>" + month[date.getMonth()] + "</span>";
                str += "<span class='day " + extraClass + "'>&nbsp;" + date.getDate() + ".</span>";
                str += "<span class='weekday " + extraClass + "'>" + days[date.getDay()] + "</span>";
                str += '</div>';
                str += '</div>';

            } else if ( countIcon && (matches = countIcon.match(/^([a-z]+)-(20[0-9][0-9])$/)) ) {
                str += "<div class='date'>";
                str += "<div class='datebg'>";
                str += "<span class='month ' style='line-height: 0.5em;'>&nbsp;</span>";
                str += "<span class='day' style='text-transform: uppercase; font-size: 1.2em;'>" + matches[1].substr(0,3) + "</span>";
                str += "<span class='weekday' style='text-transform: uppercase; font-size: 0.8em;' >" + (matches[2]) + "</span>";
                str += '</div>';
                str += '</div>';

            } else if (countIcon != null && iconmapping[countIcon]) {
                str += "<div class='countIcon'><i class='listicon " + iconmapping[countIcon] + "'></i></div>";

            } else if (count != null) {
                str += "<div class='count'>" + count + "</div>";

            } else {
                str += "<div class='count'><img src='images/wappen_header.png' style='width: auto; height: 40px' /></div>";

            }


            str += '<div class="detaildata">';
            str += '<div class="">' + title.replace(/(<([^>]+)>)/ig, "") + '</div>';
            if (subtitle) {
                str += '<span class="">' + (isSearch ? subtitle : subtitle.replace(/(<([^>]+)>)/ig, "")) + '</span>';
            }
            str += '</div>';

            if (type == "icon") {
                str += "<div class='icon'><i class='" + typeData + "'></i></div>";
            }

            str += "</a>";
            return str;
        },
        'map': function (row, hr) {
            var link =  row.hr || hr || "";
            var title = row.ti || "";
            var image = row.th || "";

            var str = "<div class='maprow' ";
            if (link) {
                str += "data-href='#/" + link.replace(/#?map#/, '') + "'";
            }
            str += ">";
            if (image) {
                str += "<img src='" + image + "'>";
            }
            str += "<h1>" + title + "</h1>";
            str += '</div>';
            return str;
        },
        'detail': function (row, hr) {
            var id = row.hr && row.hr.replace(/[^a-zA-Z0-9]/g, '') || 'random' + (new Date()).getTime();

            var str="";

            if(row.ti){
                str += "<div class='detail-" + row.ti.toLowerCase().replace(' ', '-') + "' id='" + id + "'>";
                var title = row['ti'] || "";
                str += '<h2>' + title + '</h2>';
            }else{
                var title = "";
            }

            var content = row['in'] || "";
            var info = row['if'] || "";
            var veranstalter = row.to || "";
            var ort = row.lo || "";
            var img = row.th || "";
            var link = row.hr || "";

            var eventData = {
                to: 'fa-tags',
                lo: 'fa-map-marker',
                da: 'fa-calendar',
                tm: 'fa-clock-o',
                tl: 'fa-phone',
                mb: 'fa-mobile',
                em: 'fa-envelope-o',
                hp: 'fa-link'

            };
            var eventStr = "";
            for (var eventOfs in eventData) {
                if (row[eventOfs]) {
                    var subStr = "";
                    subStr += '<i class="fa fa-li ' + eventData[eventOfs] + '"></i> ' + row[eventOfs];
                    if (eventOfs == "tm" && row.t2) {
                         subStr += ' - ' + row.t2;
                    }

                    eventStr += '<li>';
                    if (eventOfs == "tl") {
                        eventStr += '<a href="tel:'+row[eventOfs]+'">' + subStr + '</a>';
                    } else if (eventOfs == "mb") {
                        eventStr += '<a href="tel:'+row[eventOfs]+'">' + subStr + '</a>';
                    } else if (eventOfs == "em") {
                        eventStr += '<a href="mailto:'+row[eventOfs]+'">' + subStr + '</a>';
                    } else if (eventOfs == "hp") {
                        eventStr += '<a href="http://'+row[eventOfs].replace('http://', '')+'">' + subStr + '</a>';
                    } else {
                        eventStr += subStr;
                    }

                    eventStr += '</li>';
                }
            }

            var refImg = "";

            var images = content.match(/<img[^>]*>/g);
            if (images) {
                for (var imgofs = 0; imgofs < images.length; imgofs++) {
                    var oneImg = images[imgofs];
                    var extraImg = oneImg.match(/src=['"]([^'"]*)['"]/)[1];

                    if (extraImg.match(/plus.gif/)) continue;
                    if (extraImg.match(/logo.png/)) continue;
                    if (extraImg.match(/web_kein_foto.png/)) continue;
                    if (extraImg.match(/tvlogo_mit_herz.png/)) continue;

                    content = content.replace(oneImg, '<div class="imagebg"><img onload="resizeEnd()" class="image" src="' + extraImg.replace(/\?.*$/, '') + '?W=600&_as_is=J" ></div>');
                }
            }

            if (link) {
                $('#detail-share').removeClass('inactive');
                $('#detail-share').attr('href', link);
                $('#detail-share').attr('title', title.replace(/\u00AD/g, '').replace(/&shy;/g, ''));
                $('#detail-share').attr('data-img', img);
            } else {
                $('#detail-share').addClass('inactive');
                $('#detail-share').attr('href', '#');
                $('#detail-share').attr('title', title.replace(/\u00AD/g, '').replace(/&shy;/g, ''));
                $('#detail-share').attr('data-img', '');
            }


            if (row.da) {
                var dateData = row.da.split('.');
                var eventStart = dateData[2] + "-" + dateData[1] + "-" + dateData[0];
                var eventEndOfs = 1000 * 3600;
                if (row.tm) {
                    var timeData = row.tm.split(':');
                    eventStart += " " + timeData[0] + ":" + timeData[1] + ":00";

                    if (row.t2) {
                        var timeData2 = row.t2.split(':');
                        eventEndOfs = (timeData2[0] - timeData[0]) * 1000 * 3600;
                        eventEndOfs += (timeData2[1] - timeData[1]) * 1000 * 60;
                    }
                } else {
                    eventStart += " 00:00:00";
                    eventEndOfs *= 24;
                }
                var dateParts = eventStart.split(/[- :]/);
                var endDate = new Date(dateParts[0], parseInt(dateParts[1])-1, dateParts[2], dateParts[3], dateParts[4], dateParts[5]);
                endDate.setTime(endDate.getTime() + eventEndOfs);

                var eventEnd = endDate.getFullYear() + "-" + ("0" + (endDate.getMonth() + 1)).substr(-2) + "-" + ("0" + endDate.getDate()).substr(-2) + " " + ("0"+endDate.getHours()).substr(-2) + ":" + ("0"+endDate.getMinutes()).substr(-2) + ":00";

                var eventNote = "";
                var eventLocation = row.lo || "";
                var eventOwner = row.to || "";

                $('#detail-calendar').removeClass('inactive');
                $('#detail-calendar').attr('href', row.ic);
                $('#detail-calendar').attr('data-owner', eventOwner.replace(/\u00AD/g, '').replace(/&shy;/g, '').trim());
                $('#detail-calendar').attr('data-location', eventLocation.replace(/\u00AD/g, '').replace(/&shy;/g, '').trim());
                $('#detail-calendar').attr('data-start', eventStart);
                $('#detail-calendar').attr('data-end', eventEnd);
                $('#detail-calendar').attr('title', title.replace(/\u00AD/g, '').replace(/&shy;/g, '').trim());
                $('#detail-calendar').attr('data-note', eventNote.replace(/\u00AD/g, '').replace(/&shy;/g, '').trim());
            } else {
                $('#detail-calendar').addClass('inactive');
            }
            if (row.ll && row.ll.replace(/ /g, '') != "") {
                $('#detail-route').removeClass('inactive');
                $('#detail-map').removeClass('inactive');
                $('#detail-map').attr('href', '#map#' + hr);

                $('#detail-route').attr('data-location', row.ll.trim());
                $('#detail-route').attr('href', mapsUrl + '?daddr=' + row.ll);
                $('#detail-route').attr('target', '_system');
            } else {
                $('#detail-route').addClass('inactive');
                $('#detail-map').addClass('inactive');
            }

            var phone = '';
            row.ph = row.tl || row.mb || false;

            if (!row.ph) {
                var phoneAnchor = content.match(/<a[^>]*.phone.*[^>]>/);
                if (phoneAnchor) {
                    phone = phoneAnchor[0].match(/href=['"]([^'"]*)['"]/)[1];
                }
            } else {
                phone = 'tel:' + row.ph;
            }

            if (phone) {
                $('#detail-call').removeClass('inactive');
                $('#detail-call').attr('href', phone);
            } else {
                $('#detail-call').addClass('inactive');
            }

            if (eventStr) {
                str += '<ul class="fa-ul">' + eventStr + '</ul>';
            }
            if (row.lo_print) {
                str += '<div class=""><i class="icon-location"></i>' + row.lo_print + '</div>';
            }

            if (info) {
                str += '<div class="info">' + info + '</div>';
            }
            if (content) {
                str += '<div>' + content.replace(/_blank/g, '_system') + '</div>';
            }

            /*
			if (row.hr) {
				str+= '<a target="_blank" class="btn" href="'+row.hr+'">Zur Webseite</a>';
			}
			*/

            var extra = false;

            for (var key in row) {
                if (key == 'ic') continue;
                if (key == 'ti') continue;
                if (key == 'hr') continue;
                if (key == 'in') continue;
                if (key == 'if') continue;
                if (key == 'to') continue;
                if (key == 'lo') continue;
                if (key == 'lo_print') continue;
                if (key == 'th') continue;
                if (key == 'da') continue;
                if (key == 'tm') continue;
                if (key == 'ca') continue;
                if (key == 'll') continue;
                if (key == 't2') continue;
                if (key == 'op') continue;
                if (key == '_parent') continue;
                if (!extra) {
                    //str += "<hr>";
                }
                extra = true;
                //str += "<b>" + key + "</b>: " + row[key] + "<br/>";
            }
            str += "</div>";
            return str;
        }
    };

    var hashchangeTimeout = 0;
    $(window).bind('hashchange', function (event) {
        var now = Date.now();
        if (now - hashchangeTimeout < 100) {
            return;
        }
        hashchangeTimeout = now;

        historyOffset++;



        if (history.length - historyOffset > 1) {
            $('#history_next').removeClass('inactive');
        } else {
            $('#history_next').addClass('inactive');
        }

        var thisUrl = window.location.hash.replace('#', '');

        if (thisUrl.match(/^\/https?:\/\//i)) {
            historyBack();
            window.open(thisUrl.replace('/', ''), '_system');
            return;
        }

        var section = null;
        var isDetail = null;
        var isSearch = null;

        var forceMap = false;
        if (thisUrl.search("map") == 0) {
            initmap();
            $('#mapcontainer').addClass('active');

            thisUrl = thisUrl.replace('map#', '');
            if (thisUrl == "") return;

            forceMap = true;
        }

        var fullUrl = baseUrl + thisUrl + ".json";

        if (thisUrl == "/settings") {
            handleSettings();
            headerBgs();
            return;
        } else if (thisUrl.search("/search") == 0) {
            $('#searchcontainer').addClass('active');
            section = $('#search');
            isSearch = true;

            var q = decodeURIComponent(thisUrl.replace(/\/search\/?/, ''));
            $('#searchfield').val(q);
            fullUrl = searchUrl + q;

        } else if (thisUrl == "nav") {
            $('.navhide').removeClass('active');
            $('#navcontainer').addClass('active');
            $('#loading').hide();
            headerBgs();
            return;
        }



        if (!thisUrl.match(/^\//)) {
            $('#loading').hide();
            return;
        }

        if (isSearch) {
            $('#loading').show();
        }
        if (!showLoader && !comesFromHistory) {
            showLoader = setTimeout(function () {
                $('#loading').show();
            }, 200);
        }
        comesFromHistory = false;

        $.get(fullUrl, function (data) {
            if (!data.ct) {
                data.ct = "list";
                if (thisUrl.match(new RegExp("/[0-9]+$")) !== null) {
                    data.ct = "detail";
                }
                if (thisUrl.match(new RegExp("/share/[^/]+$")) !== null) {
                    data.ct = "detail";
                }
            }
            var isDetail = (data.ct == "detail");

            if (isSearch) {
                section = $('#search');
                isDetail = false;
            } else if (isDetail) {
                section = $('#detail');
            } else {
                section = $('#list');
            }

            if (!data.ty && data.ti.match(/Mülltermine/) && thisUrl.match(/termine$/)  ) {
                data.ty = "trash";
            }


            var html = '';
            map.clearAppMarker();
            var refresh = scrollList;

            if (!isSearch && !isDetail) {
                $('#listcontainer').removeClass('showbutton');
                $('#listcontainer a.showmap').attr('href', '#map').hide();
                $('#listcontainer').removeClass('showtrash');

            }

            var entryTitle = data.ti || '';
            if (!isSearch && data.d.length == 0) {
                if (isDetail) {
                    document.getElementById('detailtitle').innerHTML = '';
                } else {
                    document.getElementById('listtitle').innerHTML = '';
                }
                section.get(0).innerHTML = '';
                alert('Keine Daten gefunden!');
                historyBack();
            }

            var parentData = {
                ti: data.ti || "",
                ty: data.ty || "",
            };

            for (var key in data.d) {
                var line = "";
                var row = data.d[key];
                row._parent = parentData;

                if (row.ll) {
                    row.ll = row.ll.replace(/ /g, '');
                }


                if (isSearch) {
                    line = templates['list'](row, true);
                    refresh = scrollSearch;
                } else if (isDetail) {
                    $('#sectiondetail').scrollTop(0);
                    row.ic = baseUrl + thisUrl + '.ics';
                    if (data.ty == "trash") {
                        row.lo_print = row.lo;
                    }
                    line = templates['detail'](row, thisUrl);
                    refresh = scrollDetail;
                    document.getElementById('detailtitle').innerHTML = entryTitle || row.ti;
                } else {
                    if (!thisUrl.match('in_der_nähe$') && data.ti == "In der Nähe") {
                        row.hr = '#map#/' + row.hr;
                    }
                   line = templates['list'](row);
                    if (entryTitle) {
                        document.getElementById('listtitle').innerHTML = entryTitle;
                        entryTitle = "";
                    }
                }
                if (row.ll) {
                    if (!isDetail) {
                        $('#listcontainer').addClass('showbutton');
                        $('#listcontainer a.showmap').attr('href', '#map#' + thisUrl).show();
                        section.data('valid', 0);
                    }
                    var coords = row.ll.split(',');
                    //damit share funktioniert und nicht auf den externen weitergeleitet wird.
                    //var hr = /*row.hr || */thisUrl.replace('/', '') || '';
                    var hr = row.hr || thisUrl.replace('/', '') || '';
                    map.addAppMarker(coords[0], coords[1], templates['map'](row, hr));
                }


                if (!window.device || (window.device.platform != "Android" || !window.device.version.match(/^[23]\./))) {
                    if (data.ty == "trash" && !isDetail) {
                        $('#listcontainer').addClass('showbutton').addClass('showtrash');
                    }
                }

                html += line;
            }

            var matches = html.match(new RegExp("<script[\\s\\S.]*</script>", 'gm'));
            if (matches) {
                for (var ofs = 0; ofs < matches.length; ofs++) {
                    html = html.replace(matches[ofs], '');
                }
            }

            section.get(0).innerHTML = html;


            delete(window.js_tb_1);

            if (matches) {
                for (var ofs = 0; ofs < matches.length; ofs++) {
                    var srpt = matches[ofs].replace(/(window.open\('[^']*',')[^']*'/g, '$1_system\'');
                    section.append( srpt );
                }
            }


            refresh.scrollTo(0, 0, 0, false);

            setTimeout(function () {
                refresh.refresh();
            }, 300);

            var t = null;
            if (isSearch) {
                t = $('#searchcontainer');
            } else if (isDetail) {
                $('.detailhide').removeClass('active');
                t = $('#detailcontainer');

                /* nachdem sie ihre map mal wirklich geöffnet haben, kommt es doch weg...
                if (window.js_tb_1) {
                  $('#detail-map').attr('href', 'javascript:js_tb_1();');
                }
                */
            } else {
                $('.listhide').removeClass('active');
                t = $('#listcontainer');
            }
            if (forceMap) {
                $('#mapcontainer').addClass('active');
                if (map.appMarkers.length == 1 ) {
                    map.appMarkers[0].infowindow.open(map, map.appMarkers[0]);
                }
            }

            clearTimeout(showLoader);
            showLoader = false;
            t.addClass('active');

            headerBgs();

            /** clear timeout fix */
            setTimeout(function () {
                $('#loading').hide();
            }, 10);


        }).fail(function (obj, data, message) {
            $('#loading').show();
            var ext = function() {
                historyBack();
            };
             if (navigator.notification && navigator.notification.alert && appstarted) {
                navigator.notification.alert("Fehler bei der Internetverbindung.", ext, appName );
            } else {
                alert("Fehler bei der Internetverbindung.");
                setTimeout(ext, 2000);
            }
        });

    });

};

if (typeof cordova != "undefined") {
    appstarted = true;
    document.addEventListener("deviceready", startApp, false);
} else {
    setTimeout(function() {
        $(document).ready(startApp);
    }, 500);
}


var splashstate = function (idDone) {
    $('#splashscreen #' + idDone).addClass('done');
    $('#splashscreen #' + idDone + ' i').removeClass().addClass('fa fa-check-square-o');
    if ($('#splashscreen div:not(.done)').length == 0) {
        $('#splashscreen').hide();
    }
}

/** clone von oben! sollte ich mal fixxen ;) */
var importTrash = function (fraction, area, part) {
    var message = 'Sind Sie sicher, dass Sie alle Abfall-Termine des laufenden Jahres in Ihren persönlichen Kalender eintragen wollen?';

    if (part) {
        message = 'Sind Sie sicher, dass Sie alle '+part+' des laufenden Jahres in Ihren persönlichen Kalender eintragen wollen?';
    }

    if (!navigator.notification) {
        if (!confirm(message)) return;

        var checkArea = false;
        if (area) {
            if (confirm('Sind Sie aus dem Bereich A?')) {
                checkArea = "Altenberg";
            } else if(confirm('Sind Sie aus dem Bereich B?')){
                checkArea = "Hitzendorf";
            } else {
                return;
            }
        }
        var link = baseUrl + window.location.hash.replace('#', '')  + ".ics";
        link+= "?filter[ti]="+encodeURIComponent(fraction);
        if (checkArea) {
            link+= "&filter[lo]="+encodeURIComponent(checkArea);
        }
        window.open(link, '_system');
        return;
    }

    setTimeout(function() {
        if (area) { //disabled for tillmitsch
            navigator.notification.confirm (
                'Hitzendorf ist bei der Abfallabholung in zwei Bereiche geteilt: einen westlichen Bereich A und einen östlichen Bereich B. Wo sind Sie zu Hause?' + "\n\n" +
                'A - Altenberg, Altreiteregg, Berndorf, Höllberg, Michlbach und Neureiteregg' + "\n\n" +
                'B - Doblegg, Hitzendorf, Holzberg, Mayersdorf, Niederberg, Oberberg, Pirka',
                 function(selection) {
                    if (selection == 1) {
                        popupImportTrashAction(fraction, 'Altenberg');
                    } else if (selection == 2) {
                        popupImportTrashAction(fraction, 'Hitzendorf');
                    }
                 },            // callback to invoke with index of button pressed
                'Bereichsauswahl',           // title
                ['A','B', 'Abbrechen']     // buttonLabels
            );
        } else {
            navigator.notification.confirm(
                message,
                function(selection) {
                    if (selection == 1) {
                        popupImportTrashAction(fraction, false);
                    }
                },
                part || fraction,
                ['OK', 'Abbrechen']
            );
        }
    }, 30);
}

var popupImportTrashAction = function(fraction, area) {
    $('#popup_reminder_time').show().attr('data-fraction', fraction).attr('data-area', area ? area : '' );
    setTimeout(function() { $('#popup_reminder_time').addClass('active'); }, 150);
};

var importTrashAction = function (fraction, area, hour, minute) {

    $('#loading').show();

    var loadingCnt = {};

    $('#list > a').each(function (idx) {
        var thisUrl = $(this).attr('href').replace('#', '');

        loadingCnt[idx] = false;

        $.get(baseUrl + thisUrl + ".json", function (data) {
            for (var calOfs = 0; calOfs < data.d.length; calOfs++) {
                var row = data.d[calOfs];
                (function(row, fraction, area, hour, minute) {
                    if (row.da) {
                        var dateData = row.da.split('.');
                        var eventStart = dateData[2] + "-" + dateData[1] + "-" + dateData[0];
                        var eventEndOfs = 1000 * 3600;
                        if (row.tm) {
                            var timeData = row.tm.split(':');
                            eventStart += " " + timeData[0] + ":" + timeData[1] + ":00";

                            if (row.t2) {
                                var timeData2 = row.t2.split(':');
                                eventEndOfs = (timeData2[0] - timeData[0]) * 1000 * 3600;
                                eventEndOfs += (timeData2[1] - timeData[1]) * 1000 * 60;
                            }
                        } else {
                            eventStart += " 00:00:00";
                            eventEndOfs *= 24;

                        }
                        var dateParts = eventStart.split(/[- :]/);
                        var startDate = new Date(dateParts[0], parseInt(dateParts[1])-1, dateParts[2], dateParts[3], dateParts[4], dateParts[5]);

                        /* das war ja schon ... */
                        if (startDate.getTime() < Date.now()) return false;

                        var endDate = new Date(dateParts[0], parseInt(dateParts[1])-1, dateParts[2], dateParts[3], dateParts[4], dateParts[5]);
                        endDate.setTime(endDate.getTime() + eventEndOfs);

                        // magic troll request
                        startDate.setMinutes(0);
                        startDate.setHours(0);
                        startDate.setSeconds(0);
                        endDate.setTime(startDate.getTime());
                        endDate.setDate(endDate.getDate() + 1);
                        // magic troll end

                        var title = row.ti.replace(/\u00AD/g, '').replace(/&shy;/g, '');
                        var eventNote = "".replace(/\u00AD/g, '').replace(/&shy;/g, '');
                        var eventLocation = (row.lo || "").replace(/\u00AD/g, '').replace(/&shy;/g, '');
                        var eventOwner = (row.to || "").replace(/\u00AD/g, '').replace(/&shy;/g, '');


                        if (title.match(fraction) && (!area || eventLocation.match(area))) {
                            var calOptions = window.plugins.calendar.getCalendarOptions();

                            var offset = 24 - hour;
                            var minOfs = startDate.getMinutes() - minute;
                            if (minOfs < 0) {
                                offset--;
                                minOfs+= 60;
                            }
                            var reminderMinutes = ((((offset + startDate.getHours()) % 24) * 60) + minOfs);
                            calOptions.firstReminderMinutes = reminderMinutes;

                            var notes = eventOwner + "\n" + eventNote;

                            title = title.replace(/[\n\r]/g, ' ');
                            eventLocation = eventLocation.replace(/[\n\r]/g, ' ');
                            notes = notes.replace(/[\n\r]/g, ' ');

                            window.plugins.calendar.findEvent(title, eventLocation, notes, startDate, endDate, function(eventsFound) {
                                if (eventsFound != null && eventsFound.length > 0) {
                                    window.plugins.toast.showShortCenter(title +' ist bereits in Ihrem Kalender eingetragen');
                                    return;
                                }
                                window.plugins.calendar.createEventWithOptions(
                                    title,
                                    eventLocation,
                                    notes,
                                    startDate,
                                    endDate,
                                    calOptions,
                                    function (message) {
                                        window.plugins.toast.showShortCenter(title +' hinzugefügt');
                                    },
                                    function (message) {
                                        window.plugins.toast.showShortCenter(title +' konnte nicht hinzugefügt werden: ' + message);
                                    }
                                );
                            }, function() {
                                window.plugins.toast.showShortBottom('Error in Calendar lookup');
                            });
                        }
                    }
                })(row, fraction, area, hour, minute);
            }
        }).always(function() {
            loadingCnt[idx] = true;

            var stillLoading = false;
            for (idxOfs in loadingCnt) {
                if (!loadingCnt[idxOfs]) {
                    stillLoading = true;
                    continue;
                }
            }

            if (stillLoading) {
                $('#loading').show();
            } else {
                $('#loading').hide();
            }
        });
    });

}

var map = {
    markers: [],
    addAppMarker: function(lat, lng, title) {
        this.markers.push({
            'lat' : lat,
            'lng': lng,
            'title': title
        });
    },
    clearAppMarker: function() {
        this.markers = [];
    }
};
var initmap = function () {
    if (typeof map.appMarkers != "undefined") return;

    var mapOptions = {
        center: new google.maps.LatLng(47.035278, 15.300833),
        zoom: 16,
        disableDefaultUI: false,
        mapTypeControl: true,
        streetViewControl: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var rebuild =  map.markers;
    map = new google.maps.Map(document.getElementById("map"), mapOptions);



    var onSuccess = function (position) {
        var myPos = {
            path: 'M595.3,282.8c0,26.7-4.8,53.3-12.1,77.5c-7.3,24.2-19.4,48.5-31.5,70.3L367.5,750.5c-12.1,21.8-29.1,33.9-46,33.9s-31.5-9.7-46-33.9L91.2,428.2c-12.1-21.8-24.2-46-31.5-70.3c-7.3-24.2-12.1-50.9-12.1-77.5c0-38.8,7.3-72.7,21.8-106.6s33.9-63,58.2-87.2s53.3-43.6,87.2-58.2S282.7,6.6,321.4,6.6c38.8,0,72.7,7.3,106.6,21.8c33.9,14.5,63,33.9,87.2,58.2c24.2,24.2,43.6,53.3,58.2,87.2C588,210.1,595.3,246.5,595.3,282.8 M321.4,418.5c19.4,0,36.3-2.4,53.3-9.7c17-7.3,31.5-17,43.6-29.1s21.8-26.7,29.1-43.6c7.3-17,9.7-33.9,9.7-53.3c0-19.4-2.4-36.3-9.7-53.3c-7.3-17-17-31.5-29.1-43.6c-12.1-12.1-26.7-21.8-43.6-29.1c-17-7.3-33.9-9.7-53.3-9.7s-36.3,2.4-53.3,9.7c-17,7.3-31.5,17-43.6,29.1c-12.1,12.1-21.8,26.7-29.1,43.6c-7.3,17-9.7,33.9-9.7,53.3c0,19.4,2.4,36.3,9.7,53.3c7.3,17,17,31.5,29.1,43.6c12.1,12.1,26.7,21.8,43.6,29.1C285.1,416.1,302.1,418.5,321.4,418.5',
            fillColor: highlightLightColor,
            fillOpacity: 1,
            scale: 0.05,
            strokeColor: highlightLightColor,
            strokeWeight: 0,
            anchor: {
                x: 306,
                y: 796
            }
        };



        var myLatLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        map.myCurrentPosition = new google.maps.Marker({
            position: myLatLng,
            map: map,
            icon: myPos,
            animation: google.maps.Animation.BOUNCE,
            optimized: false
        });

        splashstate('loadingmap');
        for(var ofs = 0; ofs < rebuild.length; ofs++) {
            map.addAppMarker(rebuild[ofs].lat, rebuild[ofs].lng, rebuild[ofs].title);
        }

    };
    // onError Callback receives a PositionError object
    function onError(error) {
        splashstate('loadingmap');
        return false;
        if (error.code == 3) {
            alert('Leider hat Ihr Handy keine Positiondaten bereit gestellt!');
        } else if (error.code == 1) {
            alert('Leider darf die App nicht auf Ihre Positionsdaten zugreifen!');
        } else {
            alert('code: ' + error.code + '\n' + 'message: ' + error.message + '\n');
        }
    }
    navigator.geolocation.getCurrentPosition(onSuccess, onError, {
        enableHighAccuracy: false,
        timeout: 5000,
        maximumAge: 0
    });
    map.myBounds = new google.maps.LatLngBounds();
    map.appMarkers = [];

    var cityPins = {
        path: 'M595.3,282.8c0,26.7-4.8,53.3-12.1,77.5c-7.3,24.2-19.4,48.5-31.5,70.3L367.5,750.5c-12.1,21.8-29.1,33.9-46,33.9s-31.5-9.7-46-33.9L91.2,428.2c-12.1-21.8-24.2-46-31.5-70.3c-7.3-24.2-12.1-50.9-12.1-77.5c0-38.8,7.3-72.7,21.8-106.6s33.9-63,58.2-87.2s53.3-43.6,87.2-58.2S282.7,6.6,321.4,6.6c38.8,0,72.7,7.3,106.6,21.8c33.9,14.5,63,33.9,87.2,58.2c24.2,24.2,43.6,53.3,58.2,87.2C588,210.1,595.3,246.5,595.3,282.8 M321.4,418.5c19.4,0,36.3-2.4,53.3-9.7c17-7.3,31.5-17,43.6-29.1s21.8-26.7,29.1-43.6c7.3-17,9.7-33.9,9.7-53.3c0-19.4-2.4-36.3-9.7-53.3c-7.3-17-17-31.5-29.1-43.6c-12.1-12.1-26.7-21.8-43.6-29.1c-17-7.3-33.9-9.7-53.3-9.7s-36.3,2.4-53.3,9.7c-17,7.3-31.5,17-43.6,29.1c-12.1,12.1-21.8,26.7-29.1,43.6c-7.3,17-9.7,33.9-9.7,53.3c0,19.4,2.4,36.3,9.7,53.3c7.3,17,17,31.5,29.1,43.6c12.1,12.1,26.7,21.8,43.6,29.1C285.1,416.1,302.1,418.5,321.4,418.5',
        fillColor: highlightColor,
        fillOpacity: 1,
        scale: 0.05,
        strokeColor: highlightColor,
        strokeWeight: 0,
        anchor: {
            x: 306,
            y: 796
        }
    };

    map.addAppMarker = function (lat, lng, title) {
        var myLatlng = new google.maps.LatLng(lat, lng);
        var marker = new google.maps.Marker({
            position: myLatlng,
            map: this,
            icon: cityPins,
            optimized: false
        });
        marker.infowindow = new google.maps.InfoWindow({
            content: title
        });
        google.maps.event.addListener(marker, 'click', function () {
            for (var ofs = 0; ofs < map.appMarkers.length; ofs++) {
                map.appMarkers[ofs].infowindow.close();
            }
            marker.infowindow.open(map, marker);
        });

        this.myBounds.extend(marker.getPosition());
        map.fitBounds(this.myBounds);

        this.appMarkers.push(marker);
    };
    map.clearAppMarker = function () {
        for (var ofs = 0; ofs < this.appMarkers.length; ofs++) {
            this.appMarkers[ofs].setMap(null);
        }
        this.myBounds = new google.maps.LatLngBounds();
        if (this.myCurrentPosition) {
            this.myBounds.extend(this.myCurrentPosition.getPosition());
            map.fitBounds(this.myBounds);
        }
        this.appMarkers = [];
    };

};


$(document).on('app:mainMenu', function() {
  var updateHits = function(tx) {
  tx.executeSql('', [], function(tx, result) {
    $('#cherrycount').html(result.rows[0].numhits);
  }, function(tx, result) {
    console.log(tx, result);
  });
};

window.appDB = openDatabase('tillmitsch', '1.0', 'tillmitschdata', 2 * 1024 * 1024);
appDB.transaction(function (tx) {
   tx.executeSql('CREATE TABLE IF NOT EXISTS tillmitsch (id, name, email)', [], function(tx, result) {
     tx.executeSql('SELECT * FROM tillmitsch', [], function(tx, result) {
       console.log(tx, result);
       if (result.rows.length == 0) {
         tx.executeSql('INSERT INTO tillmitsch(id, name, email) VALUES (1, "", "");');
         $("#camera_name").val('');
         $("#camera_email").val('');
       } else {
         $("#camera_name").val(result.rows[0].name);
         $("#camera_email").val(result.rows[0].email);
       }
     }, function(tx, result) {
       console.log('err', arguments);
     });
   }, function(tx, result) {
     console.log('err', arguments);
   });
});

});

	function onPhotoInit() {
			$('#popup_camera').addClass('active');
      navigator.geolocation.getCurrentPosition(function(position) {
          $('#smallImage').attr("data-coords", position.coords.latitude + "," + position.coords.longitude );
      }, function() {

      }, {
          enableHighAccuracy: false,
          timeout: 5000,
          maximumAge: 0
      });
			return false;
	}
	function onPhotoCancel() {
			$('#popup_camera').removeClass('active');
			return false;
	}


    // Called when a photo is successfully retrieved
    //
    function onPhotoURISuccess(imageURI) {


      $('#smallImage').attr('data-src', imageURI);
      $('#smallImage').css({'background-image' : 'url(' + imageURI + ')' });
    }


    // A button will call this function
    //
    function getPhoto(source) {
      // Retrieve image file location from specified source
      navigator.camera.getPicture(onPhotoURISuccess, onPhotoFail, { quality: 50,
        destinationType: navigator.camera.DestinationType.FILE_URI,
        sourceType: source });

		return false;
    }

    function uploadPhoto() {

        //selected photo URI is in the src attribute (we set this on getPhoto)
        var imageURI = document.getElementById('smallImage').getAttribute("data-src");
        if (!imageURI) {
            alert('Bitte erst ein Bild auswählen!');
            return;
        }
		if ($("#camera_name").val().trim() == "") {
			alert('Bitte erst den Namen eingeben!');
            return;
		}
		if ($("#camera_email").val().trim() == "") {
			alert('Bitte erst eine Mail-Addresse eingeben!');
            return;
		}

    $('#camera_submit').attr('disabled', 'disabled');
        //set upload options
        var options = new FileUploadOptions();
        options.fileKey = "file";
        options.fileName = imageURI.substr(imageURI.lastIndexOf('/')+1);
        options.mimeType = "image/jpeg";

        options.params = {
            name: $("#camera_name").val(),
            mail: $("#camera_email").val(),
            message: $("#camera_message").val(),
            coords: $('#smallImage').attr('data-coords'),
			      device: device ? device.uuid : 'unknown-device'
        }

        var ft = new FileTransfer();
        ft.upload(imageURI, encodeURI("http://gemeindeapp.coreoil.net/apps/tillmitsch/server/melden.php"), onPhotoTransferSuccess, onPhotoTransferFail, options);
    }

    // Called if something bad happens.
    //
    function onPhotoFail(message) {
      alert('Fehler: ' + message);
    }

    function onPhotoTransferSuccess(r) {
       $('#camera_submit').attr('disabled', null);
        var response = JSON.parse(r.response);
        console.log(response);
        alert(response.message);

        onPhotoCancel();
        window.appDB.transaction(function (tx) {
           tx.executeSql('UPDATE tillmitsch set name = ?, email = ? WHERE id = 1;', [$("#camera_name").val(), $("#camera_email").val()], function(tx, result) {
             console.log(tx, result);
           }, function(tx, result) {
             console.log('err', arguments);
           });
        });

    }

    function onPhotoTransferFail(error) {
        $('#camera_submit').attr('disabled', null);
        alert("An error has occurred: Code = " + error.code);
        console.log("upload error source " + error.source);
        console.log("upload error target " + error.target);
    }
