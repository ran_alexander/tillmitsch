<?php

	$tries = array('gemeindeapp.coreoil.net', 'localhost', 'api.gemeinde-app.at');
	$last = end($tries);
	
	foreach($tries as $host) {
		$baseUrl = 'http://'.$host;

		if ($host != 'localhost') {
			$header = "Host: $host\r\n";
			foreach (getallheaders() as $name => $value) {
				if (in_array($name, array('Host', 'X-Forwarded-Server', 'X-Forwarded-For', 'X-Forwarded-Host'))) continue;

				$header.= "$name: $value\r\n";
			}

			$options = array(
				'http' => array(
					'method'=> "GET",
					'header'=> $header
				)
			);		
			$context = stream_context_create($options);
		} else {
			$context = null;
		}

		$content = file_get_contents($baseUrl.$_SERVER['PATH_INFO'], false, $context);
				
		if (!$content && $last != $host) continue;
		
		foreach($http_response_header as $header) {
			header($header);
		}
		
		echo $content;
		exit();
	}
